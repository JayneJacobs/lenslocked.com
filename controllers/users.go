package controllers

import (
	"fmt"
	"net/http"
	"time"

	"lenslocked.com/context"
	"lenslocked.com/email"
	"lenslocked.com/models"
	"lenslocked.com/rand"
	"lenslocked.com/views"
)

// NewUsers Only userd at   initial setup. will panic if templates cannot be parsed
func NewUsers(us models.UserService, emailer *email.Client) *Users {
	return &Users{
		NewView:      views.NewView("bootstrap", "users/new"),
		LoginView:    views.NewView("bootstrap", "users/login"),
		ForgotPwView: views.NewView("bootstrap", "users/forgot_pw"),
		ResetPwView:  views.NewView("bootstrap", "users/reset_pw"),
		us:           us,
		emailer:      emailer,
	}
}

// Users cretes a new user
type Users struct {
	NewView      *views.View
	LoginView    *views.View
	ForgotPwView *views.View
	ResetPwView  *views.View
	us           models.UserService
	emailer      *email.Client
}

// New is a method for Users type to render the user Signup Form using GET /signup method
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	var form SignupForm
	err := parseURLParams(r, &form)
	if err != nil {
		fmt.Println("Error Parsing form users.go ln38, ", err)
	}
	u.NewView.RenderView(w, r, form)
}

// SignupForm has the email and password and Name
type SignupForm struct {
	Name     string `schema:"name"`
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// Create Responds to the Create route for new user account
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form SignupForm
	vd.Yield = &form
	if err := parseFormHelper(r, &form); err != nil {
		vd.SetAlert(err)
		u.NewView.RenderView(w, r, vd)
		return
	}
	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		Password: form.Password,
	}
	if err := u.us.Create(&user); err != nil {
		vd.SetAlert(err)
		u.NewView.RenderView(w, r, vd)
		return
	}
	u.emailer.Welcome(user.Name, user.Email)

	err := u.signIn(w, &user)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	// email new user
	alert := views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "This is your New Gallary",
	}

	views.RedirectAlert(w, r, "/galleries", http.StatusFound, alert)
}

// // Delete will delete database using id
// func (u *Users) Delete(id uint) error {
// 	user := User{Model: gorm.Model{ID: id}}
// 	return u.db.Delete(&user).Error
// }

// LoginForm provides email and password
type LoginForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// Login is used to verify the email address/password and then log the user if correct.
// Post /login
func (u *Users) Login(w http.ResponseWriter, r *http.Request) {
	vd := views.Data{}
	form := LoginForm{}
	if err := parseFormHelper(r, &form); err != nil {
		vd.SetAlert(err)
		u.LoginView.RenderView(w, r, vd)
		return
	}
	fmt.Println(form.Email, form.Password, " Form Auth fields")
	user, err := u.us.Authenticate(form.Email, form.Password)

	fmt.Printf("login: user via Authenticate:: %+v\n", user)
	if err != nil {
		fmt.Println(err, "Error in Login Authenticate", form.Email, form.Password)
		switch err {
		case models.ErrNotFound:
			vd.AlertError("Invalid email address")
		default:
			vd.SetAlert(err)
		}
		u.LoginView.RenderView(w, r, vd)
		return
	}
	err = u.signIn(w, user)
	if err != nil {
		vd.SetAlert(err)
		fmt.Println("Error in Login signin ln 128", err)
		u.LoginView.RenderView(w, r, vd)
		return
	}
	alert := views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Hello " + user.Name,
	}
	views.RedirectAlert(w, r, "/galleries", http.StatusFound, alert)
	// http.Redirect(w, r, "/galleries", http.StatusFound)
}

// Logout is used to verify the email address/password and then log the user if correct.
// Post /logout
func (u *Users) Logout(w http.ResponseWriter, r *http.Request) {
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	user := context.User(r.Context())
	fmt.Printf("logout: user from context: %+v\n", user)
	token, _ := rand.RememberToken()
	user.Remember = token
	// fmt.Printf("logout: new remmeber token: %v\n", token)
	// fmt.Printf("logout: user with new remember: %+v\n", user)

	u.us.Update(user)
	fmt.Printf("logout: user after Update: %+v\n", user)
	http.Redirect(w, r, "/", http.StatusFound)
}

// ResetPwForm items for reset
type ResetPwForm struct {
	Email    string `schema:"email"`
	Token    string `schema:"token"`
	Password string `schema:"password"`
}

// CookieInfo gets the cookie info
func (u *Users) CookieInfo(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("remember_token")
	// alert, err := r.Cookie("alert_message")
	if err != nil {

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user, err := u.us.ByRemember(cookie.Value)
	fmt.Fprintln(w, "user is: ", user)
	// message := alert.Value
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintln(w, "user is: ", user)
	fmt.Fprintln(w, " ")
	// fmt.Fprintln(w, "Alert is: ", message)
	// fmt.Fprintln(w, " ")
	// fmt.Fprintln(w, "Complete Cookie:  ", message)
	// fmt.Fprintln(w, "user is: ", user)
	fmt.Fprintln(w, "Token is: ", cookie.Value)
	fmt.Fprintln(w, " ")
	fmt.Fprintln(w, "Complete Cookie:  ", cookie)
}

// InitiateReset resets user password
func (u *Users) InitiateReset(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form ResetPwForm
	vd.Yield = &form
	if err := parseFormHelper(r, &form); err != nil {
		vd.SetAlert(err)
		u.ForgotPwView.RenderView(w, r, vd)
		return
	}
	token, err := u.us.InitiateReset(form.Email)
	if err != nil {
		vd.SetAlert(err)
		u.ForgotPwView.RenderView(w, r, vd)
		return
	}
	err = u.emailer.ResetPw(form.Email, token)
	if err != nil {
		vd.SetAlert(err)
		u.ForgotPwView.RenderView(w, r, vd)
		return
	}
	views.RedirectAlert(w, r, "/reset", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Instructions for resetting password have been sent to your email address. ",
	})
}

// ResetPw Resets teh user Password
func (u *Users) ResetPw(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form ResetPwForm
	vd.Yield = &form
	if err := parseURLParams(r, &form); err != nil {
		vd.SetAlert(err)
	}
	u.ResetPwView.RenderView(w, r, vd)
}

// CompleteReset proceeds with the reset of the password
func (u *Users) CompleteReset(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form ResetPwForm
	vd.Yield = &form
	if err := parseFormHelper(r, &form); err != nil {
		vd.SetAlert(err)
		u.ResetPwView.RenderView(w, r, vd)
		return
	}
	user, err := u.us.CompleteReset(form.Token, form.Password)
	if err != nil {
		vd.SetAlert(err)
		u.ResetPwView.RenderView(w, r, vd)
		return
	}
	u.signIn(w, user)
	fmt.Println("This is the user in complete Reset: ", user.Name)
	views.RedirectAlert(w, r, "/galleries", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Your passworwd has been resetn and you are logged in. ",
	})
}

func (u *Users) signIn(w http.ResponseWriter, user *models.User) error {
	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
		fmt.Println("InSignIN: user: ", user.Name)
		err = u.us.Update(user)
		if err != nil {
			return err
		}
	}
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    user.Remember,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	return nil
}
