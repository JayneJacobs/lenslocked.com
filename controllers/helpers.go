package controllers

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/gorilla/schema"
)

func parseFormHelper(r *http.Request, dst interface{}) error {

	if err := r.ParseForm(); err != nil {
		return err
	}
	return parseValues(r.PostForm, dst)
}

func parseURLParams(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}
	return parseValues(r.Form, dst)
}

func parseValues(values url.Values, dst interface{}) error {
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	if err := dec.Decode(dst, values); err != nil {
		fmt.Println("In helpers.go: ", err)
		return err
	}
	return nil
}
