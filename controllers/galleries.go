package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/gorilla/mux"

	"lenslocked.com/context"
	"lenslocked.com/models"
	"lenslocked.com/views"
)

const (
	// IndexGalleries = "index_galleries"

	// ShowGallery is teh path for show
	ShowGallery = "show_gallery"
	// EditGallery is teh path for edits
	EditGallery     = "edit_gallery"
	maxMultipartMem = 1 << 20 // 1 megabyte
)

// NewGalleries Only ggerd at   initial setup. will panic if templates cannot be parsed
func NewGalleries(gs models.GalleryService, is models.ImageService, r *mux.Router) *Galleries {
	return &Galleries{
		New:       views.NewView("bootstrap", "galleries/new"),
		ShowView:  views.NewView("bootstrap", "galleries/show"),
		EditView:  views.NewView("bootstrap", "galleries/edit"),
		IndexView: views.NewView("bootstrap", "galleries/index"),
		gs:        gs,
		is:        is,
		r:         r,
	}
}

// Galleries cretes a new gger
type Galleries struct {
	New       *views.View
	ShowView  *views.View
	EditView  *views.View
	IndexView *views.View
	gs        models.GalleryService
	is        models.ImageService
	r         *mux.Router
}

// GalleryForm has the email and password and Title
type GalleryForm struct {
	Title string `schema:"title"`
}

// Show Responds to the get route for new gallery account
// Get galleries
func (g *Galleries) Show(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		fmt.Println(err)
		return
	}

	var vd views.Data
	vd.Yield = gallery

	g.ShowView.RenderView(w, r, vd)

}

// Index Responds to the get route for new gallery account
// GET /galleries
func (g *Galleries) Index(w http.ResponseWriter, r *http.Request) {
	user := context.User(r.Context())
	galleries, err := g.gs.ByUserID(user.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Something went wrong.", http.StatusInternalServerError)
		return
	}
	var vd views.Data
	vd.Yield = galleries
	g.IndexView.RenderView(w, r, vd)
}

// Edit Responds to the get route for new gallery account
// Get galleries
func (g *Galleries) Edit(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "gallery not found", http.StatusNotFound)
		// http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	var vd views.Data
	vd.Yield = gallery

	g.EditView.RenderView(w, r, vd)

}

// Update Responds to the get route for new gallery account
// Get galleries
func (g *Galleries) Update(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	fmt.Printf("Line 114 gallaries %v \n ", user.Name)
	if gallery.UserID != user.ID {
		http.Error(w, "gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data

	vd.Yield = gallery
	var form GalleryForm
	if err := parseFormHelper(r, &form); err != nil {
		log.Println(err)
		fmt.Println("galleries.go 41", err)
		vd.SetAlert(err)
		g.EditView.RenderView(w, r, vd)
		return
	}
	fmt.Println("gallery controller Title ", form.Title)
	gallery.Title = form.Title
	err = g.gs.Update(gallery)
	if err != nil {
		vd.SetAlert(err)
		fmt.Println(err)
		g.EditView.RenderView(w, r, vd)
		return
	}
	vd.Alert = &views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Gallery Updated",
	}

	g.EditView.RenderView(w, r, vd)

}

// ImagesUpload for gallery images
// POST /galleries/:id/images
func (g *Galleries) ImagesUpload(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	vd.Yield = gallery
	err = r.ParseMultipartForm(maxMultipartMem)
	if err != nil {
		vd.SetAlert(err)
		g.EditView.RenderView(w, r, vd)
		return
	}

	// Iterate over uploaded files to process them.
	files := r.MultipartForm.File["images"]

	for _, f := range files {
		// Open the uploaded file
		file, err := f.Open()
		if err != nil {
			vd.SetAlert(err)
			g.EditView.RenderView(w, r, vd)
			return
		}
		defer file.Close()
		err = g.is.Create(gallery.ID, file, f.Filename)
		if err != nil {
			vd.SetAlert(err)
			g.EditView.RenderView(w, r, vd)
			return
		}
	}
	url, err := g.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		log.Println(err)

		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)

}

// POST /galleries/:id/images/link
func (g *Galleries) ImageViaLink(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data
	vd.Yield = gallery
	if err := r.ParseForm(); err != nil {
		vd.SetAlert(err)
		g.EditView.RenderView(w, r, vd)
		return
	}
	files := r.PostForm["files"]

	var wg sync.WaitGroup
	wg.Add(len(files))
	for _, fileURL := range files {
		go func(url string) {
			defer wg.Done()
			resp, err := http.Get(url)
			if err != nil {
				log.Println("Failed to download the image from:", url)
				return
			}
			defer resp.Body.Close()
			pieces := strings.Split(url, "/")
			filename := pieces[len(pieces)-1]
			if err := g.is.Create(gallery.ID, resp.Body, filename); err != nil {
				log.Println("Failed to create the image from:", url)
			}
		}(fileURL)
	}
	wg.Wait()
	url, err := g.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// Delete Responds to the delete route for new gallery account
// Delete galleries
func (g *Galleries) Delete(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	fmt.Println("galleries 209 User.ID: ", user.ID)
	if gallery.UserID != user.ID {
		fmt.Println("Gallery not foutnd ln 131 galleries controllser")
		http.Error(w, "gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data

	err = g.gs.Delete(gallery.ID)
	if err != nil {
		vd.SetAlert(err)
		vd.Yield = gallery
		g.EditView.RenderView(w, r, vd)
		return
	}

	http.Redirect(w, r, "/galleries", http.StatusFound)

}

// ImageDelete  responds to the Delete request buttone in the edit template
// POST /galleries/:id/images/:filename/delete
func (g *Galleries) ImageDelete(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not foune", http.StatusNotFound)
		return
	}
	filename := mux.Vars(r)["filename"]
	i := models.Image{
		Filename:  filename,
		GalleryID: gallery.ID,
	}
	err = g.is.Delete(&i)
	if err != nil {
		var vd views.Data
		vd.Yield = gallery
		vd.SetAlert(err)
		g.EditView.RenderView(w, r, vd)
		return
	}

	url, err := g.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))

	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// Create Responds to the Create route for new gallery account
// Post galleries
func (g *Galleries) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form GalleryForm
	if err := parseFormHelper(r, &form); err != nil {
		log.Println(err)
		vd.SetAlert(err)
		g.New.RenderView(w, r, vd)
		return
	}

	user := context.User(r.Context())
	if user == nil {
		http.Redirect(w, r, "/gallaries", http.StatusFound)
	}
	fmt.Println("Create got the user:", user)
	gallery := models.Gallery{
		Title:  form.Title,
		UserID: user.ID,
	}
	if err := g.gs.Create(&gallery); err != nil {
		vd.SetAlert(err)

		g.New.RenderView(w, r, vd)
		return
	}
	url, err := g.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)

}

func (g *Galleries) galleryByID(w http.ResponseWriter, r *http.Request) (*models.Gallery, error) {
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		log.Println(err)
		http.Error(w, "Invalid ID", http.StatusNotFound)
		return nil, err
	}
	gallery, err := g.gs.ByID(uint(id))
	if err != nil {
		switch err {
		case models.ErrNotFound:
			http.Error(w, "Gallery not Found", http.StatusNotFound)
		default:
			log.Println(err)
			http.Error(w, "Something else, default case", http.StatusInternalServerError)

		}
		return nil, err
	}
	images, _ := g.is.ByGalleryID(gallery.ID)
	gallery.Images = images
	return gallery, nil
}
