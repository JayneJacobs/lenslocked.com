package controllers

import "lenslocked.com/views"

// NewStatic is a bootstrap View
func NewStatic() *Static {
	return &Static{
		Home:    views.NewView("bootstrap", "static/home"),
		Contact: views.NewView("bootstrap", "static/contact"),
	}
}

// Static sets up the static views type
type Static struct {
	Home    *views.View
	Contact *views.View
}
