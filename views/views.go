package views

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"path/filepath"

	"github.com/gorilla/csrf"

	"lenslocked.com/context"
)

var (
	LayoutDir   string = "views/layouts/"
	TemplateExt string = ".gohtml"
	TemplateDir string = "views/"
)

// NewView parses teh templates and returns a pointer to the View for main.
func NewView(layout string, files ...string) *View {
	// fmt.Println("\n Before", files)
	addTemplatePath(files)
	addTemplateExt(files)
	// fmt.Println(" After", files)
	files = append(layoutFiles(), files...)
	t, err := template.New("").Funcs(template.FuncMap{
		"csrfField": func() (template.HTML, error) {
			return "", errors.New("csrfField is not implemented")
		},
	}).ParseFiles(files...)
	if err != nil {
		fmt.Println("Error views.go ln37", err)
		panic(err)
	}

	return &View{
		Template: t,
		Layout:   layout,
	}

}

//View is the list of templates for pages
type View struct {
	Template *template.Template
	Layout   string
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v.RenderView(w, r, nil)

}

// RenderView is used to render teh view with the predefined layout.
func (v *View) RenderView(w http.ResponseWriter, r *http.Request, data interface{}) {
	w.Header().Set("Content-Type", "text/html")
	var vd Data
	switch d := data.(type) {
	case Data:
		vd = d
		// do nothing
	default:
		vd = Data{
			Yield: data,
		}
	}
	if alert := getAlert(r); alert != nil {
		fmt.Println("Found Line 73 views.go")
		vd.Alert = alert
		clearAlert(w)
	}

	vd.User = context.User(r.Context())
	fmt.Println("User ln 79 views.go: ", vd.User)
	var buf bytes.Buffer

	csrfField := csrf.TemplateField(r)
	tpl := v.Template.Funcs(template.FuncMap{
		"csrfField": func() template.HTML {
			return csrfField
		},
	})
	if err := tpl.ExecuteTemplate(&buf, v.Layout, vd); err != nil {
		log.Println("Error in RenderView ln89: ", err)
		http.Error(w, "Something went wrong. If the problem persists, please email support@lenslocked.com", http.StatusInternalServerError)
		return
	}

	io.Copy(w, &buf)
}

func layoutFiles() []string {
	files, err := filepath.Glob(LayoutDir + "*" + TemplateExt)
	if err != nil {
		panic(err)
	}
	// fmt.Println("layout files", files)
	return files
}

// addTemplatePath takes in an array of string files and adds it to the file path
//Eg the input {"home"} would result in the output
//{"views/home"} if TemplateDir == "views/"
func addTemplatePath(files []string) {
	for i, f := range files {
		files[i] = TemplateDir + f
	}
}

// addTemplateExt takes in an array of string files and adds the extention to
//each string in the slice
// Eg the input {"home"} would result in the output
// home.gohtml
func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + TemplateExt
	}
}
