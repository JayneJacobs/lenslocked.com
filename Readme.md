# Lens Locked

An awesome photo gallery application written in go.  

## Git tips
```git log```
```git add -A```  ```all untracked files```
git checkout -b (open new branch)


## Hot Reloading pilu/fresh

go get -u  github.com/pilu/fresh

type fresh in the main directory
### Default Config
Hello

To change:
fresh -c other_runner.conf

### Stashing everything since last commit
git add
git stash
gsa
git stash apply

## [Gorilla Mux](docs/gorrillamux.md)
https://courses.calhoun.io/lessons/les_wd_3.3


## [SERVEMUX](docs/servmux.md)



## [Templates](docs/templates.md)

# Bootstrap

1. HTML CSS JavaScript framework
2. Create Bootstrap layout.
3. Nest home and contact templates
4. Update Viewtype and use layouts
5. Update main.go


 browser will use the protocol used by the current page .
href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"


1. Create a navbar templates
2. Call navbar from bootstrap layouts


[Create views and use Bootstrap]("Chapter6/views/layouts/views.go")


[Signup Form]("Chapter6/views/layouts/signup.gohtml")


[Bootstrap Grid]((../docs/bootstrapGrid.md))


## [REST](../docs/REST.md)


## Controller
