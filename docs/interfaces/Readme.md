# Interfaces and chaining
  

  Interfaces: 
  what is an interface. 

     In object oriented programming , a protocol or interface for unrelated objects to communicate with each other. 

     Rules of conversation; 
     Protocol
     Operations availabe. 

     Unrelated objects; define rules between elemenets that are not coupled. 

     Interface is teh peg on a leggo. 

     Containers; 
         Docker; common interface. 
         standard size and attachement. 
         Wrapping code in a known interface. 

## Go Interface: 
 Types: Abstract / Concrete

 Concrete
    Memeory layout

    Attach Behavior: Methods

Abstract Types
    Interface
    Describes behavior. 
      By declaring methods. 


Define an area in teh space of concrete types. 

 Types taht have reader/writer methods are of type reader/writer

 Union of Interfaces: 
    Read
    Writer

    File is a readerwriter

interface{}

says nothing

largest set of things in go. 

## Why use interfaces
1. Generate algorythms
2. Hide implementation details
3. Interception points


"The bigger teh interface the weaker the abstraction"

Many Methods; 
   
Concrete type has no abstraction

### Robustness Principle

"Be conservative in what you do, be liberal in wht you accept from others"
Return Concrete types
Receive interfaces. 


TCP - what you send should be perfect: receive everything. 

Abstract Data Types: 
  Mathematical model for data types

  Defined by behaviour: 
     - possible values
     - possible operations on data of type


nil; empty(new())


type Stack interface {
    Push(v interface{}) Stack
    Pop() Stack
    Empty() bool
}

func Size(s Stack) int {
    if s.Empty() {
        return 0
    }
    return Size(s.Pop()) + 1
}

### Sortable interface
type Interface interface {
    Less(i, j int) bool
    Swap(i, j int)
    Len() int
}

func Sort(s Interface)

func Stable(s Interface)

func IsSorted(s Interface) bool

## Reader and Writer

type Reader interface {
    Read(b []byte) (int, error)

}

type Writer interface {
    Write(b []byte) (int, error)
}
 

 func Fpritnln(w Writer, ar ...interface{}) (int, error)
  func Fscan(r Reader, ar ...interface{}) (int, error)
   func Copy (w writer, r Reader) (int, error)


There is more to an abstract data type than its interface. 

Write generic algorythms. 

a) func New() *os.File
b) func New() io.ReadWriteCloser
c) func New() io.Writer
d) func New() interface{} //no claim of type that is returned. 


Return Concrete types
Receive interfaces. 


 
Hiding implementation; 
   Decouple API from concrete interface

   Context Interface. 

   Context <--- context


   cmptyCtx cancelCtx timerCtx valueCtx

   interfaces hide implemenation details

   call Dispatch f.Do()

   if f is a struct one possiblity
   If f is an interface infinite
![CallDispatch](CallDispatch.png)


Abstract types: dynamic. 

type Client struct {
    Transport RoundTripper
}

type RoundTripper interface {
    RoundTrip(*Request) (*Response, error)
}

``` go
type headers struct {
    rt http.RoundTripper
    v map[string]string
}

func (h headers) RoundTrip(r *http.Request) *http.Response {
    for k, v := ranch h.v {
        r.Header.Set(k, v)
    }
    return h.rt.RoundTrip(r)


    
}
    c := &http.Client{
        Transport: headerRoundTripper{
            rt: http.DefaultTransport,
            v: map[string]string{"foo": "bar" },
        },
    }
    res, err := c.Get("http://golang.org")
```

# Chaining Interfaces
![Chaining Interfaces](ChainingInterfaces.png)

interfaces are interception points; 

- writing generic algorithms
- hiding implementation details
- providing interception points

## Whats new

Go interfaces are satisfied implicitly. 
no "implements"

Implicit satisfaction. 

break dependencies. 
define interface where you use it. 



How do you know what satisfies an interface; 
guru; 

golang.org/su/using-guru


go doc pointer analysis


type assertions

``` go
func do(v interface{}){
    i :- v.(int) // will panic if v not int
    i, ok := v.(int) // will return false
}

func do(v interface{} {
    select v.(type) {
        case int:
           fmt.Pringln("got int %d", v)
           Default:
    }
})

func do(v interface{} {
    select v.(type) {
        case int: // t is of type int
           fmt.Pringln("got int %d", v)
       Default: // t of type interfac{}
           fmt.Println("Unrecognized type")
    }
})
```
Avoid abstract to concrete assertions
User type abstractions


### The Context Interface
```go
type Context interface {
    Done() <-chan struct{}
    Err() error
    Deadline() (deadline time.Time, ok bool)
    Value(key interface{}) interface{}
}

var Canceled, Deadline Exceeded error
var Cancelled = errors.New("context cancelled")
}
```
  
Use type assertions to classify errors. 

![TypeAssertionUpdate](TypeAssertionEvolution.png)

https://www.bilibili.com/video/BV15s411G7Tk?from=search&seid=929545923805320644

@francesc

Franciscan Poi

#### Return Structs accept Interfaces

  

