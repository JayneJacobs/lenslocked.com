# Gorrilla mux


mime types
https://www.freeformatter.com/mime-types-list.html

Gorilla web toolkit

https://www.gorillatoolkit.org/
https://github.com/gorilla


Supports named parameters
pattern matching
domain matching

### Methods
Get Post r.Methods.


### Flexibility
You can use the default router.


```r := mux.NewRouter()
// router.GET("/", Index)
r.HandleFunc("/",handlerFunc)
// mux.HandleFunc("/", handlerFunc)
http.ListenAndServe(":3000", r) //u```


## Signup form.
get.Signup
You can use gets for everything.
reads only

causes side effects.
use put/post/delete
# Steps
Install Gorilla schema
Create types
struct tags.
struct tags

#  use gorilla/schema
https://www.gorillatoolkit.org/

Standard library provides URL context.

mux
schema takes form values and convert it to a struct.


## Reflect Package

for struct tags.
Use with encoding and decoding


Not checked by compiler
Encoding package allows to define names in Form
when schema package is called in finds the name and assigns to a field.

Mapping data formats; json/go maps.

Name string `schema: "name"`
Drying up Parseing

DRY -
Steps:
1. Don't Repeat Yourself
2. Break into smaller functions.
3. wrote parseForm() to avoid repetition.


## Coding principle

Use interfaces for common functionality.

Use types and interfaces

if anything diverges; don't make it common.

Use types. Shorter code is not always easier to read.

 ## STatic controllers
 1. catchall controllers
 2. create static controllersmove templats to a dir
 3. Make views implement 'http.Handler'
