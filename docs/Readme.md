# Lens Locked

An awesome photo gallery application written in go.  

## Git tips
```git log```
```git add -A```  ```all untracked files```
git checkout -b (open new branch)


## Hot Reloading pilu/fresh

go get -u  github.com/pilu/fresh

type fresh in the main directory
### Default Config
Hello

To change:
fresh -c other_runner.conf

### Stashing everything since last commit
git add
git stash
gsa
git stash apply

## [Gorilla Mux](gorrillamux.md)
https://courses.calhoun.io/lessons/les_wd_3.3


## [SERVEMUX](servmux.md)



## [Templates](templates.md)


## 17 Redirect with Alerts

1. Persist alerts after request
2. make `Redirect` simplify
3. Update `Render` method



