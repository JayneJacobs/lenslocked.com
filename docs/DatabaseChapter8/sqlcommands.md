# Create DATABASE
psql -U <username>
CREATE DATABASE lenslocked_dev;
\c lenslocked_dev;

## Add users

```sql
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name TEXT,
    email TEXT NOT NULL
);

CREATE TABLE  orders (
  id SERIAL PRIMARY KEY,
  user_id   INT NOT NULL,
  amount INT,
  description TEXT
  );
```

```sql
  SELECT *  FROM users;

  lenslocked_dev=# SELECT *  FROM orders;
 id | user_id | amount | description
----+---------+--------+-------------
(0 rows)
```
```sql
lenslocked_dev=# DROP TABLE users;
DROP TABLE
lenslocked_dev=# DROP TABLE orders;
DROP TABLE

```
