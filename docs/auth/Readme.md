# Buildign Authentication

Don't Deviate from the norm. 
Don't use third party packages. 

Understand to avoid making mistakes. 

## Custom Requirements
2 factor / monthly password resets. 
web API 

## Save Time

Rails Devise. 

Customizing is difficult. 

Third party packages can slow you down. 

## Save Money; 

Free tiers are limited. 

Build once and reuse across systems. 

## Do not reinvent the wheel. 

## SSL TLS

HTTPs. only send encrypted passwords. 

- Never send passwords unencrypted. 

- Cookie Theft. 

## Hashing

encrypt passwords 
Don't leak passwords

If you can decrypt a password it is not done correctly

Store a hash. 

password --> Hashing Function --> Hash

Compare hash. 

One way hash. 
You cant reverse it. 

impossible to reverse. 

Drop box, encrypts the hash. Key, 

Hash password before creating the user. 

godoc.org/!/subrepo
https://godoc.org/-/subrepo

https://godoc.org/golang.org/x/crypto/bcrypt

#[Bcrypt](bcrypt.md)
