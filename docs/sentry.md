# sentry.io/welcome

	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://bf8e755fe93f44fda0cbeb6e4986a831@o494734.ingest.sentry.io/5566332",
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
	defer sentry.Flush(2 * time.Second)

	sentry.CaptureMessage("It works!")

    https://sentry.io/organizations/comcast-9q/alerts/rules/?project=5566332