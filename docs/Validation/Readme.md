# Validation

Meet minimum requirements

example 
- email spelling and uniqueness 
- password length

## Normalization

formating as expected

ex. 
 - email lowercase
 - Hashing passwords and Remember Tokens

Normalization - Validation tightly Linked. 

- normaliz then validate; 

- validate before hashing .

Read write from DB w gorm

Try to validate data; 

Too much in User Service. 

## Split code into layers
single responsiblity. 

Many design options

Use as first pass/redesign. 

![UserService](UserService.png)


![Authenticate](authenticate.png)


![Normalize](NormalizeRW.png)