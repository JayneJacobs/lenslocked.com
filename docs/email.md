## Email Users
- Mailgun <https://www.mailgun.com>
- `net/smtp` vs third party
- Mailgun package:
  
  ```go
  go get gopkg.in/mailgun/mailgun-go.v1
  ```

  ```go
  import mailgun gopkg.in/mailgun/mailgun-go.v1
  from := "@demo@sandboxea3f45a3140d4aa9b79eb09f6802a3f5.mailgun.org"
  to := "Jayne Jacobs <jaynejacobs@comcast.net>"
  subject := "Welcome to LensLocked.com!"
  text := ` Hi there
  Welcome to Lenslocked
  Thank you for joining
  Jayne`
  html := `Hi there!<br/>
  Welcome to <a href="https:lenslocked.selfmanagedmusician.com"></a>!`
  mst := mailgun.NewMessage(from, subject, text, to)
  cfg := LoadConfgi(...)
  mgClient := mailgun.Newmailgun()
  mgCfg := cfg.Mailgun
  mgClient := mailgun.Newmailgun(mgCfg.Domain, mgCfg.APIKey, mgCfg.PublicApi)
  
  respMsg, id, err := mgClient.Send(msg)
  
  ```

  ### Steps:

  1. Create account
  2. config
  3. Create message
  4. format address
  5. HTML messages
  6. Send with mailgun client
  7. Create email package
  8. add emailer to controllers. 

sandboxea3f45a3140d4aa9b79eb09f6802a3f5.mailgun.org

curl -s --user 'api:2e32e914054ba82ea57f1306d2826206-c50a0e68-8f9ad5c8' \
	 https://api.mailgun.net/v3/sandboxea3f45a3140d4aa9b79eb09f6802a3f5.mailgun.org/messages \
	 -F from='Mailgun Sandbox <postmaster@sandboxea3f45a3140d4aa9b79eb09f6802a3f5.mailgun.org>' \
	 -F to='Jayne Jacobs <jaynejacobs@comcast.net>' \
	 -F subject='Hello Jayne Jacobs' \
	 -F text='Congratulations Jayne Jacobs, you just sent an email with Mailgun!  You are truly awesome!'

# You can see a record of this email in your logs: https://app.mailgun.com/app/logs.

# You can send up to 300 emails/day from this sandbox server.
# Next, you should add your own domain so you can send 10000 emails/month for free.

curl -s --user 'api:2e32e914054ba82ea57f1306d2826206-c50a0e68-8f9ad5c8' \
    https://api.mailgun.net/v3/lenslocked.selfmanagedmusician.com/messages \
    -F from='Excited User <postmaster@lenslocked.selfmanagedmusician.com>' \
    -F to='Jayne Jacobs <jaynejacobs@comcast.net>'  \
    -F subject='Hello Jayne Jacobs' \
    -F text='Testing some Mailgun awesomeness!'