# REST
Representational STate Transfer


HTTP Requests:
* GET
* POST - creating
* PUT  - update (PATCH) replace an object
* DELETE - cancel


* POST /gallaries/:identifier/archive
 - PATH REFLECT METHOD
In forms use Post
<form class="form-horizontal" action="/signup" method="POST">

PUT and Create do not behave as expected.

 Use post because GETs are cached and can be scraped.
 Will not follow POSTs.

 # Sign Up Form

  POST / Users
  GET /signup
  POST /signup
  DELETE


  - Controllers; mapped to resources
   - Users Controller
   - Gallary Controllers


## How To


Mapping resources.


Steps:
 1. Actions == Handlers
 2. Create Users Controllers
 3. Signup Page Route.
 4.  Move view/signup.gohtml to views/users/new.gohtml
5. NewView in `New()`
7. NewView to avoid global variables.
8. Connect router and users Controller

Resources:
  HTTP request

  get - WriteHeader
  post - write CREATE
  put - updating PATCH(less common partial put)
  delete -


prefix http:/www.mysite.com/
  get /gallaries/new
  post /gallaries /NewUsers /gallaries/:identier/archive
  get /gallaries/:identifier
  put /gallaries/:identifier  
  delete /gallaries/:identifier

Controllers in MVC.

  -
