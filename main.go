package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"lenslocked.com/controllers"
	"lenslocked.com/email"
	"lenslocked.com/middleware"
	"lenslocked.com/models"
	"lenslocked.com/rand"
)

func main() {
	// err := sentry.Init(sentry.ClientOptions{
	// 	Dsn: "https://bf8e755fe93f44fda0cbeb6e4986a831@o494734.ingest.sentry.io/5566332",
	// })
	// if err != nil {
	// 	log.Fatalf("sentry.Init: %s", err)
	// }
	// defer sentry.Flush(2 * time.Second)

	// sentry.CaptureMessage("It works!")

	boolPtr := flag.Bool("prod", false, "Provide this flag in production. This ensures that a .config file ")
	flag.Parse()
	cfg := LoadConfig(*boolPtr)
	// fmt.Println("Dropbox", cfg.Dropbox)
	dbCfg := SetPostgresConfig()
	services, err := models.NewServices(
		models.WithGorm(dbCfg.Dialect(), dbCfg.ConnectionInfo()),
		models.WithLogMode(!cfg.IsProd()),
		models.WithUser(cfg.UserPwPepper, cfg.HmacSecretKey),
		models.WithGallery(),
		models.WithImage(),
		models.WithOAuth(),
	)
	must(err)
	defer services.Close()
	services.DestructiveReset()
	services.CreateTable()

	mgCfg := cfg.Email
	emailer := email.NewClient(
		email.WithSender("Lenslocked Support", "support@lenslocked.selfmanagedmusician.com.mailgun.org"),
		email.WithMailgun(mgCfg.Domain, mgCfg.APIKey, mgCfg.PublicAPIkey),
	)

	r := mux.NewRouter()
	staticC := controllers.NewStatic()
	usersC := controllers.NewUsers(services.User, emailer)
	galleriesC := controllers.NewGalleries(services.Gallery, services.Image, r)
	configs := make(map[string]*oauth2.Config)
	configs[models.OAthDropbox] = &oauth2.Config{
		ClientID:     cfg.Dropbox.ID,
		ClientSecret: cfg.Dropbox.Secret,
		Endpoint: oauth2.Endpoint{
			AuthURL:  cfg.Dropbox.AuthURL,
			TokenURL: cfg.Dropbox.TokenURL,
		},
		RedirectURL: "http://lenslocked.selfmanagedmusician.com/oauth/dropbox/callback",
	}
	oauthsC := controllers.NewOAuths(services.OAuth, configs)

	random32, err := rand.Bytes(32)
	must(err)
	mwCSRF := csrf.Protect(random32, csrf.Secure(cfg.IsProd()))

	userMw := middleware.User{
		UserService: services.User,
	}
	requireUserMw := middleware.RequireUser{
		User: userMw,
	}

	r.Handle("/", staticC.Home).Methods("GET")
	r.Handle("/contact", staticC.Contact).Methods("GET")
	r.HandleFunc("/signup", usersC.New).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")
	r.Handle("/login", usersC.LoginView).Methods("GET")
	r.HandleFunc("/login", usersC.Login).Methods("POST")
	r.HandleFunc("/logout",
		requireUserMw.ApplyFn(usersC.Logout)).
		Methods("POST")
	r.Handle("/forgot", usersC.ForgotPwView).Methods("GET")
	r.HandleFunc("/forgot", usersC.InitiateReset).Methods("POST")
	r.HandleFunc("/reset", usersC.ResetPw).Methods("GET")
	r.HandleFunc("/reset", usersC.CompleteReset).Methods("POST")
	r.HandleFunc("/cookietest",
		usersC.CookieInfo).Methods("GET")
	//Assets

	assetsHandler := http.FileServer(http.Dir("./assets"))
	assetsHandler = http.StripPrefix("/assets", assetsHandler)
	r.PathPrefix("/assets/").Handler(assetsHandler)

	// Images
	imageHandler := http.FileServer(http.Dir("./images"))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images",
		imageHandler))

	// Gallery
	// Validate User Middleware
	r.Handle("/galleries",
		requireUserMw.ApplyFn(galleriesC.Index)).
		Methods("GET")
	r.Handle("/galleries/new",
		requireUserMw.Apply(galleriesC.New)).
		Methods("GET")
	r.HandleFunc("/galleries",
		requireUserMw.ApplyFn(galleriesC.Create)).
		Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/edit",
		requireUserMw.ApplyFn(galleriesC.Edit)).
		Methods("GET").
		Name(controllers.EditGallery)
	r.HandleFunc("/galleries/{id:[0-9]+}/update",
		requireUserMw.ApplyFn(galleriesC.Update)).
		Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/delete",
		requireUserMw.ApplyFn(galleriesC.Delete)).
		Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}",
		galleriesC.Show).
		Methods("GET").
		Name(controllers.ShowGallery)
	r.HandleFunc("/galleries/{id:[0-9]+}/images",
		requireUserMw.ApplyFn(galleriesC.ImagesUpload)).
		Methods("POST")
	// /galleries/:id/images/:filename/delete
	r.HandleFunc("/galleries/{id:[0-9]+}/images/{filename}/delete",
		requireUserMw.ApplyFn(galleriesC.ImageDelete)).
		Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/images/link",
		requireUserMw.ApplyFn(galleriesC.ImageViaLink)).
		Methods("POST")

	fmt.Printf("Starting server on :%d...\n", cfg.Port)
	// connect := fmt.Sprintf(":%d", cfg.Port)

	// Oauth Routes
	r.HandleFunc("/oauth/dropbox/{service:[a-z]+}/connect", requireUserMw.ApplyFn(oauthsC.Connect))

	r.HandleFunc("/oauth/dropbox/{service:[a-z]+}/test", requireUserMw.ApplyFn(oauthsC.Callback))

	r.HandleFunc("/oauth/dropbox/{service:[a-z]+}/callback", requireUserMw.ApplyFn(oauthsC.DropboxTest))

	http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), mwCSRF(userMw.Apply(r)))
}

func must(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}
