package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

// PostgresConfig is a struct with DB items
type PostgresConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Dbname   string `json:"dbname"`
	Password string `json:"password"`
}

// Dialect returns db type
func (c PostgresConfig) Dialect() string {
	return "postgres"
}

// ConnectionInfo returns the connection string
func (c PostgresConfig) ConnectionInfo() string {
	if c.Password == "" {
		return fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable", c.Host, c.Port, c.User, c.Dbname)
	}
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s  sslmode=disable", c.Host, c.Port, c.User, c.Password, c.Dbname)
}

// SetPostgresConfig returns PostgresConfig
func SetPostgresConfig() PostgresConfig {
	return PostgresConfig{
		Host:     "localhost",
		Port:     5432,
		User:     "postgres",
		Dbname:   "lenslocked_prod",
		Password: "pyztgryz",
	}
}

// MailgunConfig struct
type MailgunConfig struct {
	APIKey       string `json:"api_key"`
	PublicAPIkey string `json:"public_api_key"`
	Domain       string `json:"domain" `
}

// OAuthConfig is the Dropbox authentication type
type OAuthConfig struct {
	ID       string `json:"id"`
	Secret   string `json:"secret"`
	AuthURL  string `json:"auth_url"`
	TokenURL string `json:"token_url"`
}

// Config config.json
type Config struct {
	// Port in json
	Port          int            `json:"port"`
	Env           string         `json:"env"`
	UserPwPepper  string         `json:"userPwPepper"`
	HmacSecretKey string         `json:"hmacSecretKey"`
	Database      PostgresConfig `json:"database"`
	Email         MailgunConfig  `json:"mailgun"`
	Dropbox       OAuthConfig    `json:"dropbox"`
}

// IsProd returns prod
func (c Config) IsProd() bool {
	return c.Env == "prod"
}

// DefaultConfig sets defaults
func DefaultConfig() Config {
	return Config{
		Port:          3000,
		Env:           "dev",
		UserPwPepper:  "secret-myrandom-string",
		HmacSecretKey: "secret-hmac-key",
		Database:      SetPostgresConfig(),
	}
}

// LoadConfig sets the config
func LoadConfig(configReq bool) Config {
	f, err := os.Open("config.json")
	if err != nil {
		if configReq {
			log.Println("LoadingConfig", err)
			panic(err)
		}
		fmt.Println("Using default config")
		return DefaultConfig()

	}
	var c Config
	dec := json.NewDecoder(f)
	err = dec.Decode(&c)
	if err != nil {
		log.Println("Decoding to config ln93", err)
		panic(err)
	}
	fmt.Println("Successfully loaded config.json")
	return c
}
