package rand

import (
	"crypto/rand"
	"encoding/base64"
)

// RememberTokenBytes 32 at least
const RememberTokenBytes = 32

// Bytes will generate random bytes of return an error
func Bytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// String takes an int and returns a random base64 byte Slice  encoded string
func String(nBytes int) (string, error) {
	b, err := Bytes(nBytes)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

// RememberToken genterates RememberTokens as base 64 byte string
func RememberToken() (string, error) {
	return String(RememberTokenBytes)
}

// NBytes returns number of Bytesused in base64
// URL encoded string
func NBytes(base64String string) (int, error) {
	b, err := base64.URLEncoding.DecodeString(base64String)
	if err != nil {
		return -1, err
	}
	return len(b), nil
}
