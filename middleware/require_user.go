package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"lenslocked.com/context"
	"lenslocked.com/models"
)

// User type verifies login
type User struct {
	models.UserService
}

// Apply  appllies middleware handlers
func (mw *User) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

// ApplyFn appllies middleware handlers
func (mw *User) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// if the path has an asset or an image we skip cookie lookup
		path := r.URL.Path
		if strings.HasPrefix(path, "/assets/") ||
			strings.HasPrefix(path, "/images/") {
			next(w, r)
			return
		}

		//if user logged in
		// t := time.Now()
		// fmt.Println("Fake request timer:   ", t)
		cookie, err := r.Cookie("remember_token")

		if err != nil {
			next(w, r)
			// http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, err := mw.UserService.ByRemember(cookie.Value)
		if err != nil {
			next(w, r)
			return
		}
		ctx := r.Context()
		ctx = context.WithUser(ctx, user)
		fmt.Println("User in mw", user)
		r = r.WithContext(ctx)

		next(w, r)
		// fmt.Println("Reqeust duration", time.Since(t))

	})

}

// RequireUser  authenticates user with Remember in cookie assumes taht User has been run
type RequireUser struct {
	User
}

// Apply  appllies middleware handlers
func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

// ApplyFn appllies middleware handlers
func (mw *RequireUser) ApplyFn(next http.HandlerFunc) http.HandlerFunc {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//if user logged in
		user := context.User(r.Context())
		fmt.Printf("In RequireUser.ApplyFn 73 %v \n", user)
		if user == nil {
			fmt.Println("User is nil in ApplyFn :", user)
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		next(w, r)

	})

}
