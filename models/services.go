package models

import (
	// postgress added for the datatbase
	_ "github.com/jinzhu/gorm/dialects/postgres"
	gorm "gopkg.in/jinzhu/gorm.v1"
)

// ServicesConfig type is a functionthat reeceives a ponter to SErvices and returns an error
type ServicesConfig func(*Services) error

// WithGorm takes a string for db connection and returns a compete connection
func WithGorm(dialect, connectionInfo string) ServicesConfig {
	return func(s *Services) error {

		db, err := gorm.Open(dialect, connectionInfo)
		if err != nil {
			return err
		}
		s.db = db
		return nil
	}
}

// WithUser search with User email address.
func WithUser(pepper, hmacKey string) ServicesConfig {
	return func(s *Services) error {
		s.User = NewUserService(s.db, pepper, hmacKey)
		return nil
	}
}

// WithGallery returns Service config after addign db
func WithGallery() ServicesConfig {
	return func(s *Services) error {
		s.Gallery = NewGalleryService(s.db)
		return nil
	}
}

// WithImage adds image service
func WithImage() ServicesConfig {
	return func(s *Services) error {
		s.Image = NewImageService()
		return nil
	}
}
func WithOAuth() ServicesConfig {
	return func(s *Services) error {
		s.OAuth = NewOAuthService(s.db)
		return nil
	}
}

// WithLogMode ad Log Mode
func WithLogMode(mode bool) ServicesConfig {
	return func(s *Services) error {
		s.db.LogMode(mode)

		return nil
	}
}

// NewServices  takes a connection string and returns a pointer to Services
func NewServices(cfgs ...ServicesConfig) (*Services, error) {

	var s Services
	for _, cfg := range cfgs {
		if err := cfg(&s); err != nil {
			return nil, err
		}
	}
	return &s, nil
	// db, err := gorm.Open(dialect, connectionInfo)

}

// Services stryct containes GalleryService and UserService
type Services struct {
	Gallery GalleryService
	User    UserService
	Image   ImageService
	OAuth   OAuthService
	db      *gorm.DB
}

// Close for db
func (sv *Services) Close() error {
	return sv.db.Close()
}

// DestructiveReset Drops a databases if we need to
func (sv *Services) DestructiveReset() error {
	err := sv.db.DropTableIfExists(&User{}, &Gallery{}, &OAuth{}, &pwReset{}).Error
	if err != nil {
		return err
	}
	return sv.CreateTable()
}

// CreateTable sets up the database
func (sv *Services) CreateTable() error {
	return sv.db.AutoMigrate(&User{}, &Gallery{}, &OAuth{}, &pwReset{}).Error
}
