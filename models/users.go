package models

import (

	//The following installs the driver

	"fmt"
	"regexp"
	"strings"
	"time"
	// _ blank to load database driver

	"golang.org/x/crypto/bcrypt"
	gorm "gopkg.in/jinzhu/gorm.v1"
	"lenslocked.com/hash"
	"lenslocked.com/rand"
)

// User  struct Name Email Password
type User struct {
	gorm.Model
	Name         string
	Email        string `gorm:"not null;unique_index"`
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
	Remember     string `gorm:"-"`
	RememberHash string `gorm:"not null;unique_index"`
}

// UserDB is an interface used to interact with Users Database
// For single User queries
type UserDB interface {
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByRemember(token string) (*User, error)

	//Methods for altereing users
	Update(user *User) error
	Delete(id uint) error
	Create(user *User) error
}

// UserService is the set of methods to manuipulate
// and work with user model
type UserService interface {
	// Authenticate will verify the provided email address
	// and password are correct.
	//If so, the user corresponding to the email will be returned
	// If not you will receive an error
	Authenticate(email, password string) (*User, error)
	InitiateReset(email string) (string, error)
	CompleteReset(token, newPw string) (*User, error)
	UserDB
}

// NewUserService creates the database connection
func NewUserService(db *gorm.DB, pepper, hmacSecretKey string) UserService {
	ug := &userGorm{db}

	hmac := hash.NewHMAC(hmacSecretKey)
	uv := newUserValidator(ug, hmac, pepper)

	return &userService{
		UserDB:    uv,
		pepper:    pepper,
		pwResetDB: newPwResetValidator(&pwResetGorm{db}, hmac),
	}
}

var _ UserService = &userService{}

// userService start the db.
type userService struct {
	UserDB
	pepper    string
	pwResetDB pwResetDB
}

// Authenticate user with Post email password
func (us *userService) Authenticate(email, password string) (*User, error) {
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err
	}
	err = bcrypt.CompareHashAndPassword([]byte(foundUser.PasswordHash), []byte(password+us.pepper))
	if err != nil {
		fmt.Println("Error in Authenicate function: ", err)
		switch err {
		case bcrypt.ErrMismatchedHashAndPassword:
			return nil, ErrInvalidPassword
		default:
			return nil, err
		}
	}
	fmt.Println("Found user in Authenticate: ", foundUser)
	return foundUser, nil
}

func (us *userService) InitiateReset(email string) (string, error) {
	user, err := us.ByEmail(email)
	if err != nil {
		return "", err
	}
	pwr := pwReset{
		UserID: user.ID,
	}
	if err := us.pwResetDB.Create(&pwr); err != nil {
		return "", err
	}
	return pwr.Token, nil
}

func (us *userService) CompleteReset(token, newPw string) (*User, error) {
	pwr, err := us.pwResetDB.ByToken(token)
	if err != nil {
		if err == ErrNotFound {
			return nil, ErrTokenInvalid
		}
		return nil, err
	}
	if time.Now().Sub(pwr.CreatedAt) > (12 * time.Hour) {
		return nil, ErrTokenInvalid
	}

	user, err := us.ByID(pwr.UserID)
	user.Password = newPw
	err = us.Update(user)
	if err != nil {
		return nil, err
	}
	err = us.pwResetDB.Delete(pwr.ID)
	if err != nil {
		fmt.Println("Error in user.go model Reset ln 133", err)
		return nil, err
	}
	return user, nil
}

type userValFunc func(*User) error

func runUserValFuncs(user *User, fns ...userValFunc) error {
	for _, fn := range fns {
		if err := fn(user); err != nil {
			return err
		}
	}
	return nil
}

var _ UserDB = &userValidator{}

func newUserValidator(udb UserDB, hmac hash.HMAC, pepper string) *userValidator {
	return &userValidator{
		UserDB:     udb,
		hmac:       hmac,
		emailRegex: regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,16}$`),
		pepper:     pepper,
	}
}

type userValidator struct {
	UserDB
	hmac       hash.HMAC
	emailRegex *regexp.Regexp
	pepper     string
}

// ByRemember finds a member by a remember string and returns the pointer to the user
// Teh token should be haseed.
func (uv *userValidator) ByRemember(token string) (*User, error) {
	user := User{
		Remember: token,
	}
	if err := runUserValFuncs(&user, uv.hmacRemember); err != nil {

		return nil, err

	}

	return uv.UserDB.ByRemember(user.RememberHash)
}

// ByEmail  will normalize email address before calling ByEmail on db
func (uv *userValidator) ByEmail(email string) (*User, error) {
	user := User{
		Email: email,
	}
	err := runUserValFuncs(
		&user,
		uv.normalizeEmail,
	)
	if err != nil {
		return nil, err
	}
	return uv.UserDB.ByEmail(user.Email)
}

// Create will create a user and backfill data like id
func (uv *userValidator) Create(user *User) error {

	err := runUserValFuncs(user,
		uv.passwordRequired,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.setRememberIfUnset,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requrieEmail,
		uv.emailFormat,
		uv.emailIsAvaialable)
	if err != nil {
		return err
	}
	return uv.UserDB.Create(user)
}

// Update will hash a rememberHash and create a user and backfill data like id
func (uv *userValidator) Update(user *User) error {
	err := runUserValFuncs(user,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.rememberMinBytes,
		uv.normalizeEmail,
		uv.requrieEmail,
		uv.emailFormat,
		uv.emailIsAvaialable)
	if err != nil {
		return err
	}
	return uv.UserDB.Update(user)
}

// Delete will delete user using id
func (uv *userValidator) Delete(id uint) error {
	var user User
	user.ID = id
	err := runUserValFuncs(&user, uv.idGreaterThan(0))
	if err != nil {
		return err
	}
	return uv.UserDB.Delete(id)
}

// bcryptPassword will hash a user's password with a
// predefined pepper (userPwPepper) and bcrypt if the
// Password field is not the empty string
func (uv *userValidator) bcryptPassword(user *User) error {
	if user.Password == "" {
		return nil
	}
	pwBytes := []byte(user.Password + uv.pepper)
	hashedBytes, err := bcrypt.GenerateFromPassword(pwBytes, bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.PasswordHash = string(hashedBytes)
	user.Password = ""
	return nil
}

func (uv *userValidator) hmacRemember(user *User) error {
	if user.Remember == "" {
		return nil
	}

	user.RememberHash = uv.hmac.Hash(user.Remember)
	return nil
}

func (uv *userValidator) setRememberIfUnset(user *User) error {
	if user.Remember != "" {
		return nil
	}
	token, err := rand.RememberToken()

	if err != nil {
		return err
	}
	user.Remember = token
	return nil
}

func (uv *userValidator) normalizeEmail(user *User) error {
	user.Email = strings.ToLower(user.Email)
	user.Email = strings.TrimSpace(user.Email)
	return nil
}

func (uv *userValidator) requrieEmail(user *User) error {
	if user.Email == "" {
		return ErrEmailRequired
	}
	return nil
}

func (uv *userValidator) emailFormat(user *User) error {
	if user.Email == "" {
		return nil
	}
	if !uv.emailRegex.MatchString(user.Email) {
		return ErrEmailInvalid
	}
	return nil
}

func (uv *userValidator) rememberMinBytes(user *User) error {
	if user.Email == "" {
		return nil
	}
	n, err := rand.NBytes(user.Remember)
	if err != nil {
		return err
	}
	if n < 32 {
		return ErrRememberBytesTooShort
	}
	return nil
}

func (uv *userValidator) rememberHashRequired(user *User) error {
	if user.PasswordHash == "" {
		return ErrRememberHashIsRequired
	}
	return nil
}

func (uv *userValidator) idGreaterThan(n uint) userValFunc {
	return userValFunc(func(user *User) error {
		if user.ID <= n {
			return ErrInvalidID
		}
		return nil
	})
}

func (uv *userValidator) emailIsAvaialable(user *User) error {
	exists, err := uv.ByEmail(user.Email)
	if err == ErrNotFound {
		return nil
	}
	if err != nil {
		return err
	}
	if user.ID != exists.ID {
		return ErrEmailTaken
	}
	return nil
}

func (uv *userValidator) passwordRequired(user *User) error {
	if user.Password == "" {
		return ErrPasswordIsRequired
	}
	return nil
}

func (uv *userValidator) passwordHashRequired(user *User) error {
	if user.PasswordHash == "" {
		return ErrPasswordHashIsRequired
	}
	return nil
}

func (uv *userValidator) passwordMinLength(user *User) error {
	if user.Password == "" {
		return nil
	}
	if len(user.Password) < 8 {
		return ErrPasswordTooShort
	}
	return nil
}

var _ UserDB = &userGorm{}

// UserGorm is teh gormDB type
// func (uv *userValidator) ByID(id uint) (*User, error) {
// 	if id <= 0 {
// 		return nil, errors.New("Invalid id")

// 	}
// 	return uv.UserDB.ByID(id)
// }
type userGorm struct {
	db *gorm.DB
}

// ByEmail look up by id provided
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
func (ug *userGorm) ByEmail(email string) (*User, error) {
	var user User
	db := ug.db.Where("email = ?", email)
	err := first(db, &user)
	return &user, err
}

// ByID look up by id provided
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
func (ug *userGorm) ByID(id uint) (*User, error) {
	var user User
	db := ug.db.Where("id = ?", id)
	err := first(db, &user)

	return &user, err
}

// ByRemember finds a member by a remember string and returns the pointer to the user
// Teh token should be haseed.
func (ug *userGorm) ByRemember(rememberHash string) (*User, error) {

	var user User
	err := first(ug.db.Where("remember_hash = ?", rememberHash), &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// Create will create the provided user and backfill data
// like the ID, CreatedAt, and UpdatedAt fields.
func (ug *userGorm) Create(user *User) error {
	return ug.db.Create(user).Error
}

// Update will create a user and backfill data like id
func (ug *userGorm) Update(user *User) error {

	return ug.db.Save(user).Error
}

// Delete will delete the user with the provided ID
func (ug *userGorm) Delete(id uint) error {
	user := User{Model: gorm.Model{ID: id}}
	return ug.db.Delete(&user).Error
}

func first(db *gorm.DB, dst interface{}) error {
	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}
	return nil
}
