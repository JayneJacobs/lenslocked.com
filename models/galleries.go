package models

import (
	"log"

	gorm "gopkg.in/jinzhu/gorm.v1"
)

// Gallery db model
type Gallery struct {
	gorm.Model
	UserID uint    `gorm:"not_null;index"`
	Title  string  `gorm:"not_null"`
	Images []Image `gorm:"-"`
}

// ImagesSplitN sizes images
func (g *Gallery) ImagesSplitN(n int) [][]Image {
	ret := make([][]Image, n)
	for i := 0; i < n; i++ {
		ret[i] = make([]Image, 0)
	}
	// % is the remainder operator in Go
	// eg:
	//    0%3 = 0
	//    1%3 = 1
	//    2%3 = 2
	//    3%3 = 0
	//    4%3 = 1
	//    5%3 = 2

	for i, img := range g.Images {
		bucket := i % n
		ret[bucket] = append(ret[bucket], img)
	}
	return ret
}

// GalleryService interface is GalleryDB
type GalleryService interface {
	GalleryDB
}

// GalleryDB interface is implemented with Create(gallery *Gallery)
type GalleryDB interface {
	ByID(id uint) (*Gallery, error)
	ByUserID(userID uint) ([]Gallery, error)
	Create(gallery *Gallery) error
	Update(gallery *Gallery) error
	Delete(id uint) error
}

// NewGalleryService takes the DB and returns a pointer to the Create() Gallery Service
func NewGalleryService(db *gorm.DB) GalleryService {
	return &galleryService{
		GalleryDB: &galleryValidator{&galleryGorm{db}},
	}
}

type galleryService struct {
	GalleryDB
}

type galleryValidator struct {
	GalleryDB
}

// Create will create a gallery and backfill data like id
func (gv *galleryValidator) Create(gallery *Gallery) error {

	err := runGalleryValFuncs(gallery,
		gv.titleRequired,
		gv.userIDRequired,
		// gv.rememberMinBytes,
		// gv.hmacRemember,
	)

	if err != nil {
		return err
	}
	return gv.GalleryDB.Create(gallery)
}

// Create will create a gallery and backfill data like id
func (gv *galleryValidator) Update(gallery *Gallery) error {

	err := runGalleryValFuncs(gallery,
		gv.titleRequired,
		gv.userIDRequired,
		// gv.rememberMinBytes,
		// gv.hmacRemember,
	)

	if err != nil {
		return err
	}
	return gv.GalleryDB.Update(gallery)
}

// Delete will delete user using id
func (gv *galleryValidator) Delete(id uint) error {
	var gallery Gallery
	gallery.ID = id
	if id <= 0 {
		return ErrInvalidID
	}

	// err := runGalleryValFuncs(&gallery, gv.idGreaterThan(0))
	// if err != nil {
	// 	return err
	// }
	return gv.GalleryDB.Delete(id)
}

func (gv *galleryValidator) titleRequired(g *Gallery) error {
	if g.Title == "" {
		return ErrTitleRequired
	}
	return nil
}
func (gv *galleryValidator) userIDRequired(g *Gallery) error {
	if g.UserID <= 0 {
		return ErrGalleryIDRequired
	}
	return nil

}

// GalleryDB is a pointer to  the Gallery Database
var _ GalleryDB = &galleryGorm{}

type galleryGorm struct {
	db *gorm.DB
}

// ByID look up by id provided
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
func (gg *galleryGorm) ByID(id uint) (*Gallery, error) {
	var gallery Gallery
	db := gg.db.Where("id = ?", id)
	err := first(db, &gallery)

	return &gallery, err
}

func (gg *galleryGorm) ByUserID(userID uint) ([]Gallery, error) {
	var galleries []Gallery
	err := gg.db.Where("user_id = ?", userID).Find(&galleries).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return galleries, nil
}

func (gg *galleryGorm) Create(gallery *Gallery) error {
	return gg.db.Create(gallery).Error
}

func (gg *galleryGorm) Update(gallery *Gallery) error {
	return gg.db.Save(gallery).Error
}

func (gg *galleryGorm) Delete(id uint) error {
	gallery := Gallery{Model: gorm.Model{ID: id}}
	return gg.db.Delete(&gallery).Error
}

type galleryValFunc func(*Gallery) error

func runGalleryValFuncs(gallery *Gallery, fns ...galleryValFunc) error {
	for _, fn := range fns {
		if err := fn(gallery); err != nil {
			return err
		}
	}
	return nil
}
