package models

import "strings"

//Users Errors
const (
	// ErrNotFound is returned when a resource cannot be found
	// in the database.
	ErrNotFound modelError = "models: resource not found"

	// ErrInvalidPassword is returned when an invalid password
	// is used when attempting to authenticate a user.
	ErrInvalidPassword modelError = "models: incorrect password provided"

	// ErrEmailRequired is returned when an email address is
	// not provided when creating a user
	ErrEmailRequired modelError = "models: email address is required"

	// ErrEmailInvalid is returned when an email address provided
	// does not match any of our requirements
	ErrEmailInvalid modelError = "models: email address is not valid"

	// ErrEmailTaken is returned when an update or create is attempted
	// with an email address that is already in use.
	ErrEmailTaken modelError = "models: email address is already taken"

	// ErrPasswordTooShort is returned if Password is too short
	ErrPasswordTooShort modelError = "models: email address is t oo short"

	// ErrTokenInvalid is returned if the token is not valids
	ErrTokenInvalid modelError = "models: token is not valid"

	// ErrInvalidID is returned when an invalid ID is provided
	// to a method like Delete.
	ErrInvalidID privateError = "models: ID provided was invalid"

	// ErrPasswordIsRequired is returned if Password is not entered
	ErrPasswordIsRequired modelError = "models: password is required"

	// ErrPasswordHashIsRequired is returned if Password is not entered
	ErrPasswordHashIsRequired privateError = "models: password is required"

	// ErrRememberBytesTooShort is returned if the 64bit encoded Remember token string is less than 32 bytes
	ErrRememberBytesTooShort privateError = "models: encoded Remember bytes too short"

	// ErrRememberHashIsRequired returned when RememberHash does not exist
	ErrRememberHashIsRequired privateError = "models: encoded RememberHASH required"

	ErrUserIDRequired  privateError = " models: user ID required"
	ErrServiceRequired privateError = "models: service  required for OATH"
	ErrDBRequired      privateError = "models: DB required for OATH"
)

//Galleries Errors
const (
	// ErrTitleRequired is returned when a Title is not found
	// in the database.
	ErrTitleRequired modelError = "models: title not found"

	// ErrUserIDRequired is returned when an ID is not provided
	// to a method like Delete.
	ErrGalleryIDRequired privateError = "models: ID is required"
)

type modelError string

// type publicError string

func (e modelError) Error() string {
	return string(e)
}

func (e modelError) Public() string {

	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] = strings.Title(split[0])
	return strings.Join(split, "")
}

type privateError string

func (e privateError) Error() string {
	return string(e)
}
