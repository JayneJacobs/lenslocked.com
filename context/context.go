package context

import (
	"context"
	"fmt"

	"lenslocked.com/models"
)

const (
	userKey privateKey = "user"
)

type privateKey string

// WithUser  allwos contaxt to be passed
func WithUser(ctx context.Context, user *models.User) context.Context {
	fmt.Println("userkey user in WithUser", userKey, user)

	return context.WithValue(ctx, userKey, user) // no type safety
}

// User  converts string temp to typ *User
func User(ctx context.Context) *models.User {

	if temp := ctx.Value(userKey); temp != nil {
		if user, ok := temp.(*models.User); ok {
			fmt.Println("user in context ln27", user)
			return user
		}
		fmt.Println("no user in context")
	}
	fmt.Println("no user returned in context.go")
	return nil
}
